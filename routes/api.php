<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\WebhookController;
use App\Http\Controllers\API\ParamController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/webhook/{appid}/{appsecret}', [WebhookController::class, 'store'])->name('api.webhook.store');
Route::get('/param/{appid}/{appsecret}', [ParamController::class, 'index'])->name('api.param.index');