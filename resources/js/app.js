import './bootstrap';
import Swal from 'sweetalert2';
import moment from 'moment';
import Chart from 'chart.js/auto';

window.Swal = Swal;
window.moment = moment;
window.Chart = Chart;